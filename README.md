# Queries that metaDB should respond

1. Get properties of the instrument that was used to execute an analysis (e.g. RunId, instrument type, instrument name, status...)
2. Get parameters that were used to run an analysis
3. Get properties of the output of a process:
    - Example: Get properties of last sequencing result for blood sample X (e.g. RunId, Flow, Lane, Index, LabId, Labprep...)
    - Get files of certain type for analysis X
4. Get last successful analysis of any type to a blood sample


# Notes

* In this context the words _"analysis"_ and _"process"_ may be used interchangeably
* The set of things used as inputs or outputs of a process may be called or _"artifact"_. 
* A process may receive/produce one or more artifacts as input/output.
* A process may use all or a subset of artifacts produced by a previous process.
* A process may receive as input artifacts from one or more processes.
