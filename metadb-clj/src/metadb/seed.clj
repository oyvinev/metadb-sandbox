(ns metadb.seed
  (:require [clojure.instant :as inst]
            [jsonista.core :as j]
            [tick.core :as t]))


(defn- timestamp
  "Variadic function to generate java date instance timestamp"
  ([]
   (-> (t/instant)
       str
       inst/read-instant-date))
  ([dur-in-hours]
   (-> (t/>> (t/instant) (t/new-duration dur-in-hours :hours))
       str
       inst/read-instant-date)))

;; Generate test seeds on db
(def seeds
  [ ;;sequencing
   {:db/id -1
    :process/tags ["sequencing" "novaseqx"]
    :process/started_on (timestamp)
    :process/ended_on (timestamp 3)
    :entity/output [{:db/id -2
                      :artifact/tags ["sequencing_result"]
                      :artifact/metadata
                      (j/write-value-as-string {:sex :female
                                                :lims-id "ABCD1234"}) 
                      :entity/output
                      [{:db/id -3 :file/type "fastq" :file/path "/data/durable/FILE_1.fastq.gz"}
                       {:db/id -4 :file/type "fastq" :file/path "/data/durable/FILE_2.fastq.gz"}]
                      :artifact/created-on (timestamp 3)}]}
   ;; pipeline
   {:db/id -5
    :process/tags ["pipeline" "vcpipe"]
    :process/started_on (timestamp 3)
    :process/ended_on (timestamp 6)
    :entity/output [{:db/id -6
                      :artifact/tags ["vcpipe_result"]
                      :artifact/metadata
                      (j/write-value-as-string {:sex :female
                                                :lims-id "ABCD1234"}) 
                      :entity/output
                      [{:db/id -7 :file/type "vcf" :file/path "/data/durable/FILE_3.vcf"}
                       {:db/id -8 :file/type "gvcf" :file/path "/data/durable/FILE_4.gvcf"}]
                      :artifact/created-on (timestamp 6)}]    
    :entity/input [-2]}

   ;; process with artifact input 
   {:db/id -9
    :process/tags ["pipeline" "triopipe"]
    :process/started_on (timestamp 7)
    :entity/input [-6]}
   
   ;;add to previous process: ended_on and outputs
   {:db/id -9
    :process/ended_on (timestamp 9)
    :entity/output [{:db/id -10
                      :artifact/tags ["triopipe_result"]
                      :artifact/metadata (j/write-value-as-string {:pedigree [123 345 678]})
                      :entity/output
                      [{:db/id -11 :file/type "bam" :file/path "/data/durable/FILE_5.bam"}]}]}

   {:db/id -12
    :process/tags ["postprocessing" "triopipe"]
    :process/started_on (timestamp 10)
    :entity/input [-10]
    :process/ended-on (timestamp 12)
    :entity/output [{:db/id -13
                      :artifact/tags ["alert_process_done"]
                      :artifact/metadata (j/write-value-as-string {:pedigree [123 345 678]})
                      :entity/output
                      [{:db/id -14 :file/type "txt" :file/path "/data/durable/REPORT_FILE_1.txt"}]}]}
   ])


(comment
  
  )
