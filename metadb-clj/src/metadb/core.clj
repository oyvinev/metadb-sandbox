(ns metadb.core
  (:require [datalevin.core :as d] 
            [metadb.schema :as schema] 
            [metadb.seed :as seed]))

;; SPEC (Queries)
;; 1- Get file of a certain type for an analysis X
;; 2- Get files of certain type for the last successful analysis of genetic study X
;; 3- Get members of the same family of a proband
;; 4- Get storage location of a set of files required to do an analysis X

;; Establish connection to db
(def conn (d/get-conn "/tmp/datalevin/mydb" schema/schema))

(comment
  ;; Transact some data  
  (d/transact! conn seed/seeds)

  ;; QUERIES

  ;; Find process with tags 'sequencing' and 'novaseqx', incl. artifacts
  (-> (ffirst (d/q '[:find (pull ?e [* {:entity/output
                                        [* {:entity/output [*]}]}])
                     :in $ ?tags
                     :where
                     [?e :process/tags ?tags]]
                   (d/db conn)
                   ["sequencing" "novaseqx"])))

  (d/q '[:find (pull ?e [* {:entity/output ...}])
         :in $ ?tags
         :where
         [?e :process/tags ?tags]]
       (d/db conn)
       ["sequencing" "novaseqx"])

  ;; Find fastq files, including the processes which have the files as output (backward search)
  (-> (d/q '[:find (pull ?e [* {[:entity/_output :as :output-from] ...}])
             :in $ ?type
             :where
             [?e :file/type ?type]]
           (d/db conn)
           "fastq"))

  ;; find vcf and gvcf files with processes and artifacts
  ;; NB! Does not produce all attributes of recursed entities, 
  ;; some end up as only db/id. TODO: Experiment further
  (-> (d/q '[:find (pull ?e [* 
                             {[:entity/_output :as output-from] ...} 
                             {[:entity/_input :as input-to] ...}])
             :in $ [?type ...]
             :where
             [?e :file/type ?type]]
           (d/db conn)
           ["vcf" "gvcf"]))

  ;; find triopipe
  (-> (d/q '[:find (pull ?e [*
                             {[:entity/input :as :inputs] ...}
                             {[:entity/output :as :outputs] ...}])
             :in $ ?tags
             :where
             [?e :process/tags ?tags]]
           (d/db conn)
           ["pipeline" "triopipe"]))

  ;; rules (NB! INITIAL TESTING - USELESS AS IS)
  (-> (d/q '[:find ?artifact
             :in $ %
             :where
             [?artifact :artifact/tags ?tags]
             (not (has-objects? ?artifact))]
           (d/db conn)
           '[[(has-objects? ?artifact)
              [?artifact _]
              ;; rule: add :artifact/objects key with false value if not exists 
              #_[(get-else $ % :artifact/objects false) ?u]
              ;; rule: find entities which do not have :artifact/objects key
              [(missing? $ % :entity/output)]]]
           #_["sequencing_result"]))


  ;; Retracting the attribute of an entity
  (d/transact! conn [[:db/retract 6 :artifact/metadata nil]])

  ;; Close DB connection
  (d/close conn)

  )
