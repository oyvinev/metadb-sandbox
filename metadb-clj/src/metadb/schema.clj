(ns metadb.schema)

(def schema  
  {:artifact/tags {:db/cardinality :db.cardinality/one
                   :db/valueType :db.type/tuple
                   :db/tupleType :db.type/string}   
   :artifact/created-on {:db/valueType :db.type/instant}
   ;; An artifact can have many object, typically files
   
   :process/tags {:db/cardinality :db.cardinality/one
                  :db/valueType :db.type/tuple
                  :db/tupleType :db.type/string}
   :process/started-on {:db/valueType :db.type/instant}
   :process/ended-on {:db/valueType :db.type/instant}

   ;; A process has only one input and/or one output, namely an artifact
   :entity/input {:db/cardinality :db.cardinality/many
                   :db/valueType :db.type/ref} 
   :entity/output {:db/cardinality :db.cardinality/many
                     :db/valueType :db.type/ref}})
