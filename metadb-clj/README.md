# Datalog Database with Clojure
## Environment Setup
- NB! We don't use docker for this playground, since it does not provide anything strictly needed for development
1. [Install Clojure](https://clojure.org/guides/install_clojure)
- NB! Note the prerequisites: Java, bash, curl, rlwrap

## Running the Development Environment
### VSCode
1. Install [Calva](https://calva.io/) extension
2. Open project root in VSCode
3. Open `src/metadb/core.clj` in editor
4. `Ctrl+Alt+c` `Ctrl+Alt+j` to start a REPL, make sure to use the alias `jvm-base`, which is needed for datalevin (the db library), and is defined in `deps.edn`
- Select `deps.edn`
![](resources/images/start-repl.png)
- Select `jvm-base`
![](resources/images/select-deps-alias.png)
- You should then see the following when REPL has been started
![](resources/images/jacked-in.png)
- Verify that the REPL is working, by evaluating, e.g. `(+ 22 20)` in REPL command prompt:
![](resources/images/test-repl.png)
5. Place cursor in `core.clj` file and load/evaluate file into REPL: `Ctrl+Alt+c Enter`. You will see the following in the REPL window:
```sh
clj꞉user꞉>
; Evaluating file: core.clj
nil
clj꞉metadb.core꞉>
```
- When the file has been evaluated/loaded successfully, we already have a connection to the db bound to `conn` in the source file.
- The connection to the db has also loaded the schema defined in `schema.clj` onto the db.
6. Move cursor into `comment` block
- We can use the comment block as a testing ground, the individual forms within the block will not be evaluated upon loading the file into REPL, so we can test each form step by step.
- Seed the database by evaluating the first form within the comment block by pressing `Ctrl+Enter` with the cursor situated at the end of the line, after closing parenthesis. The seed definitions are found in `seed.clj`. We can see what happened on the db in the REPL window:
```
 :tx-data
 [#datalevin/Datom [1 :process/tags ["sequencing" "novaseqx"]]
  #datalevin/Datom [1 :process/started_on #inst "2023-04-11T11:34:13.314-00:00"]
  #datalevin/Datom [1 :process/ended_on #inst "2023-04-11T14:34:13.314-00:00"]
  #datalevin/Datom [2 :artifact/tags ["sequencing_result"]]
  #datalevin/Datom [2 :artifact/metadata "{\"sex\":\"female\",\"lims-id\":\"ABCD1234\"}"]
  #datalevin/Datom [3 :file/type "fastq"]
  #datalevin/Datom [3 :file/path "/data/durable/FILE_1.fastq.gz"]
  #datalevin/Datom [2 :entity/output 3]
  #datalevin/Datom [4 :file/type "fastq"]
  #datalevin/Datom [4 :file/path "/data/durable/FILE_2.fastq.gz"]
  #datalevin/Datom [2 :entity/output 4]
  #datalevin/Datom [2 :artifact/created-on #inst "2023-04-11T14:34:13.314-00:00"]
  #datalevin/Datom [1 :entity/output 2]
  #datalevin/Datom [5 :process/tags ["pipeline" "vcpipe"]]
  #datalevin/Datom [5 :process/started_on #inst "2023-04-11T14:34:13.314-00:00"]
  #datalevin/Datom [5 :process/ended_on #inst "2023-04-11T17:34:13.314-00:00"]
  #datalevin/Datom [6 :artifact/tags ["vcpipe_result"]]
  #datalevin/Datom [6 :artifact/metadata "{\"sex\":\"female\",\"lims-id\":\"ABCD1234\"}"]
  #datalevin/Datom [7 :file/type "vcf"]
  #datalevin/Datom [7 :file/path "/data/durable/FILE_3.vcf"]
  #datalevin/Datom [6 :entity/output 7]
  #datalevin/Datom [8 :file/type "gvcf"]
  #datalevin/Datom [8 :file/path "/data/durable/FILE_4.gvcf"]
  #datalevin/Datom [6 :entity/output 8]
  #datalevin/Datom [6 :artifact/created-on #inst "2023-04-11T17:34:13.314-00:00"]
  #datalevin/Datom [5 :entity/output 6]
  #datalevin/Datom [5 :entity/input 2]
  #datalevin/Datom [9 :process/tags ["pipeline" "triopipe"]]
  #datalevin/Datom [9 :process/started_on #inst "2023-04-11T18:34:13.315-00:00"]
  #datalevin/Datom [9 :entity/input 6]
  #datalevin/Datom [9 :process/ended_on #inst "2023-04-11T20:34:13.315-00:00"]
  #datalevin/Datom [10 :artifact/tags ["triopipe_result"]]
  #datalevin/Datom [10 :artifact/metadata "{\"pedigree\":[123,345,678]}"]
  #datalevin/Datom [11 :file/type "bam"]
  #datalevin/Datom [11 :file/path "/data/durable/FILE_5.bam"]
  #datalevin/Datom [10 :entity/output 11]
  #datalevin/Datom [9 :entity/output 10]
  #datalevin/Datom [12 :process/tags ["postprocessing" "triopipe"]]
  #datalevin/Datom [12 :process/started_on #inst "2023-04-11T21:34:13.315-00:00"]
  #datalevin/Datom [12 :entity/input 10]
  #datalevin/Datom [12 :process/ended-on #inst "2023-04-11T23:34:13.315-00:00"]
  #datalevin/Datom [13 :artifact/tags ["alert_process_done"]]
  #datalevin/Datom [13 :artifact/metadata "{\"pedigree\":[123,345,678]}"]
  #datalevin/Datom [14 :file/type "txt"]
  #datalevin/Datom [14 :file/path "/data/durable/REPORT_FILE_1.txt"]
  #datalevin/Datom [13 :entity/output 14]
  #datalevin/Datom [12 :entity/output 13]]
```

> **_NOTE:_** The naming `:entity/output` in the schema is bad, and should change. Basically, what we want to be able to recursively traverse trees easily, is to have a node/children like attribute for all "main entities", in fact, it could be `:node/children`, and then we could specify an attribute on the child whether it is input or output...?

> **_NOTE:_** For dealing with parenthesis in LISP dialects, there is a tool installed with Calva called `Paredit`. This tool exposes lots of keyboard combinations to perform structural editing. You can see the available ones via command palette. "Barfing" is especially useful to move a form into our out from nested parenthesis.


### Queries
[`datalevin`](https://github.com/juji-io/datalevin), the library used here, is based on [`DataScript`](https://github.com/tonsky/datascript), which is an in-memory db and  [`Datalog`](https://en.wikipedia.org/wiki/Datalog) query engine. Datalog is "a declarative logic programming language, where a program consists of facts and rules, where the former are statements held to be true, and the latter says how to deduce new facts from known facts." Datomic is a Clojure dialect of Datalog. I mention it here, because its documentation ([data model](https://docs.datomic.com/cloud/whatis/data-model.html#datalog)/[queries](https://docs.datomic.com/cloud/query/query-executing.html)) is useful to refer to for understanding the underlying technology used herein, even if not all Datomic features are present in `datalevin`.

#### Example: Find processes with tags 'sequencing' and 'novaseqx'
An explanation of the below pull statement:
- `->`: Thread-first macro, this simply indicates that the result of the immediately following statement is used as a first argument of any following function calls. For instance, we could process the data further after `(d/q...)` function call, e.g. massaging the query result set.
- `(d/q...)`: A `datalevin` function to query the db, with the specific query passed in as first argument, then the connection to the db, and any parameters used in the query.
- `$`: The db
- `?e`: A binding to the entities in result. All names with preceding `?` are bindings.
- `[?e :process/tags ?tags]`: Binds the value of the entity's `:process/tags` attribute to `?tags`
```
(d/q '[:find (pull ?e [* {:entity/output
		                 [* {:entity/output [*]}]}])
		:in $ ?tags
		:where
        [?e :process/tags ?tags]]
        (d/db conn)
        ["sequencing" "novaseqx"])
			 
;; gives the following:

{:db/id 1,
 :process/tags ["sequencing" "novaseqx"],
 :entity/output
 [{:db/id 2,
   :artifact/tags ["sequencing_result"],
   :artifact/created-on #inst "2023-04-11T14:34:13.314-00:00",
   :entity/output
   [{:db/id 3, :file/type "fastq", :file/path "/data/durable/FILE_1.fastq.gz"}
    {:db/id 4, :file/type "fastq", :file/path "/data/durable/FILE_2.fastq.gz"}],
   :artifact/metadata "{\"sex\":\"female\",\"lims-id\":\"ABCD1234\"}"}],
 :process/started_on #inst "2023-04-11T11:34:13.314-00:00",
 :process/ended_on #inst "2023-04-11T14:34:13.314-00:00"}
```
This tells the query engine to pull entities (`?e`) with all the attributes of `?e`, all attributes of any artifact entities contained in the `:entity/output` attribute of `?e`, and finally any entities with their attributes contained in `:entity/output` key of artifact entity. This is similar to describing the paths and edges of a graph like in graphql, effectively generating a tree out of the entities stored on the db. If we did not specify that we wanted all child entities with all their attributes, we would stop at `?e`'s artifacts, presented as `:db/id`s:

The `:in` syntax is used by the constraint `:where`, in this case the `:process/tags` key, bound to `?tags` in the query. They are passed in as a vector of strings.

```
'[:find (pull ?e [*]) 
             :in $ ?tags
             :where
             [?e :process/tags ?tags]]

;; would give the following 

[{:db/id 1,
   :process/tags ["sequencing" "novaseqx"],
   :entity/output [#:db{:id 2}],
   :process/started_on #inst "2023-04-11T10:41:23.809-00:00",
   :process/ended_on #inst "2023-04-11T13:41:23.810-00:00"}]
```
If we compare this to the schema, we find that the attributes `:entity/output` and `:entity/input` are defined as refs, i.e.:
```
(def schema  
  {:artifact/tags {:db/cardinality :db.cardinality/one
                   :db/valueType :db.type/tuple
                   :db/tupleType :db.type/string}   
   :artifact/created-on {:db/valueType :db.type/instant}
   ;; An artifact can have many object, typically files
   
   :process/tags {:db/cardinality :db.cardinality/one
                  :db/valueType :db.type/tuple
                  :db/tupleType :db.type/string}
   :process/started-on {:db/valueType :db.type/instant}
   :process/ended-on {:db/valueType :db.type/instant}

   ;; A process has only one input and/or one output, namely an artifact
   :entity/input {:db/cardinality :db.cardinality/many
                   :db/valueType :db.type/ref} 
   :entity/output {:db/cardinality :db.cardinality/many
                     :db/valueType :db.type/ref}})
```
Thus, datalog knows of any entities related to the `?e`s that we pull with our query via they refs.

```
(d/q '[:find (pull ?e [* {:entity/output ...}])
         :in $ ?tags
         :where
         [?e :process/tags ?tags]]
       (d/db conn)
       ["sequencing" "novaseqx"])
	   
;;=> 
	   
([{:db/id 1,
   :process/tags ["sequencing" "novaseqx"],
   :entity/output
   [{:db/id 2,
     :artifact/tags ["sequencing_result"],
     :artifact/created-on #inst "2023-04-11T14:34:13.314-00:00",
     :entity/output
     [{:db/id 3, :file/type "fastq", :file/path "/data/durable/FILE_1.fastq.gz"}
      {:db/id 4, :file/type "fastq", :file/path "/data/durable/FILE_2.fastq.gz"}],
     :artifact/metadata "{\"sex\":\"female\",\"lims-id\":\"ABCD1234\"}"}],
   :process/started_on #inst "2023-04-11T11:34:13.314-00:00",
   :process/ended_on #inst "2023-04-11T14:34:13.314-00:00"}])
```

#### Example: Backwards query starting with vcf and gvcf files
```
(-> (d/q '[:find (pull ?e [* 
                             {[:entity/_output :as output-from] ...} 
                             {[:entity/_input :as input-to] ...}])
             :in $ [?type ...]
             :where
             [?e :file/type ?type]]
           (d/db conn)
           ["vcf" "gvcf"]))
		   
;; => 

([{:db/id 7,
   :file/type "vcf",
   :file/path "/data/durable/FILE_3.vcf",
   output-from
   [{:db/id 6,
     :artifact/tags ["vcpipe_result"],
     :artifact/created-on #inst "2023-04-11T17:34:13.314-00:00",
     :entity/output [#:db{:id 7} #:db{:id 8}],
     :artifact/metadata "{\"sex\":\"female\",\"lims-id\":\"ABCD1234\"}",
     input-to
     [{:db/id 9,
       :process/tags ["pipeline" "triopipe"],
       :entity/input [#:db{:id 6}],
       :entity/output [#:db{:id 10}],
       :process/started_on #inst "2023-04-11T18:34:13.315-00:00",
       :process/ended_on #inst "2023-04-11T20:34:13.315-00:00"}],
     output-from
     [{:db/id 5,
       :process/tags ["pipeline" "vcpipe"],
       :entity/input [#:db{:id 2}],
       :entity/output [#:db{:id 6}],
       :process/started_on #inst "2023-04-11T14:34:13.314-00:00",
       :process/ended_on #inst "2023-04-11T17:34:13.314-00:00"}]}]}]
 [{:db/id 8,
   :file/type "gvcf",
   :file/path "/data/durable/FILE_4.gvcf",
   output-from
   [{:db/id 6,
     :artifact/tags ["vcpipe_result"],
     :artifact/created-on #inst "2023-04-11T17:34:13.314-00:00",
     :entity/output [#:db{:id 7} #:db{:id 8}],
     :artifact/metadata "{\"sex\":\"female\",\"lims-id\":\"ABCD1234\"}",
     input-to
     [{:db/id 9,
       :process/tags ["pipeline" "triopipe"],
       :entity/input [#:db{:id 6}],
       :entity/output [#:db{:id 10}],
       :process/started_on #inst "2023-04-11T18:34:13.315-00:00",
       :process/ended_on #inst "2023-04-11T20:34:13.315-00:00"}],
     output-from
     [{:db/id 5,
       :process/tags ["pipeline" "vcpipe"],
       :entity/input [#:db{:id 2}],
       :entity/output [#:db{:id 6}],
       :process/started_on #inst "2023-04-11T14:34:13.314-00:00",
       :process/ended_on #inst "2023-04-11T17:34:13.314-00:00"}]}]}])
```

### TODO
- Write a query rule for recursing backwards from artifact object to sequencing process
- Fix naming of common attribute for recursive queries
- Get full entities (with all attributes) in recursive queries
- Create more elaborate db seeds
