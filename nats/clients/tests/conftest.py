import nats
import pytest_asyncio


@pytest_asyncio.fixture(autouse=True)
async def cleanup_streams():
    nc = await nats.connect("nats://localhost:4222")
    js = nc.jetstream()
    try:
        await js.delete_stream("test_stream")
    except nats.js.errors.NotFoundError:
        pass

    yield
    try:
        await js.delete_stream("test_stream")
    except nats.js.errors.NotFoundError:
        pass
    await nc.close()
