import asyncio

import typer

from clients.nats import NATSManager

app = typer.Typer()


def fibo(n):
    if n < 2:
        return n
    return fibo(n - 1) + fibo(n - 2)


@app.command()
def subscriber(subject: str, queue: str = None):
    async def printer(msg):
        print(msg)
        await msg.ack()

    async def _main():
        mgr = await NATSManager().create()
        await mgr.add_stream("test_stream", ["fibo.even", "fibo.odd"])
        await mgr.subscribe("test_stream", subject, printer, queue=queue)
        return await mgr.run_forever()

    asyncio.run(_main())


@app.command()
def publisher(start: int, count: int = 1):
    async def _main():
        mgr = await NATSManager().create()
        print(await mgr.add_stream("test_stream", ["fibo.even", "fibo.odd"]))

        for i in range(start, start + count):
            num = fibo(i)
            if num % 2 == 0:
                await mgr.publish("fibo.even", str(num).encode())
            else:
                await mgr.publish("fibo.odd", str(num).encode())
        await mgr.stop()

    asyncio.run(_main())


@app.command()
def kv_get(bucket: str, key: str):
    async def _main():
        mgr = await NATSManager().create()
        kv = await mgr.get_kv_bucket(bucket)
        print(await kv.get(key))

    asyncio.run(_main())


@app.command()
def kv_put(bucket: str, key: str, value: str):
    async def _main():
        mgr = await NATSManager().create()
        kv = await mgr.get_kv_bucket(bucket)
        await kv.put(key, value.encode())

    asyncio.run(_main())


@app.command()
def kv_watch(bucket: str, key: str):
    async def _main():
        mgr = await NATSManager().create()
        kv = await mgr.get_kv_bucket(bucket)
        watcher = await kv.watch(key)
        while True:
            try:
                update = await anext(watcher)
            except StopAsyncIteration:
                pass
            await asyncio.sleep(0.1)
            print(update)

    asyncio.run(_main())
