import json
import os
import random
from dataclasses import dataclass, field
from datetime import datetime, timedelta
from itertools import count
from typing import List

random.seed(0)

@dataclass
class Process:
    type: str
    meta: dict
    started_at: datetime
    finished_at: datetime
    inputs: List["Artifact"] = field(default_factory=list)
    outputs: List["Artifact"] = field(default_factory=list)
    index: int = field(default_factory=count(1).__next__)

@dataclass
class Artifact:
    type: str
    meta: dict
    index: int = field(default_factory=count(1).__next__)


def random_date(start, end):
    """
    This function will return a random datetime between two datetime 
    objects.
    """
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = random.randrange(int_delta)
    return start + timedelta(seconds=random_second)


root_processes = 10000

#process_names = ["sequencing", "alignment", "qc", "variant_calling", "joint_calling", "annotation"]
#artifact_types = ["aligned reads", "raw_reads"]
process_outputs = {
    "sequencing": ["raw_reads"],
    "alignment": ["aligned_reads"],
    "qc": ["qc_report", "log"],
    "dragen_variant_calling": ["vcf"],
    "dragen_cnv_variant_calling": ["vcf"],
    "gatk_variant_calling": ["vcf"],
    "dragen_joint_calling": ["vcf", "log"],
    "annotation": ["vcf"],
}

process_inputs = {
    "sequencing": [],
    "alignment": ["raw_reads"],
    "qc": ["aligned_reads"],
    "dragen_variant_calling": ["aligned_reads"],
    "dragen_cnv_variant_calling": ["aligned_reads"],
    "gatk_variant_calling": ["aligned_reads"],
    "dragen_joint_calling": ["vcf", "vcf", ...],
    "annotation": ["vcf"],
}

artifact_index = 0
process_index = 0


def CSVWriter():
    artifacts = ".dockerfiles/db/init/seed_data_artifacts.csv"
    processes = ".dockerfiles/db/init/seed_data_processes.csv"
    process_inputs = ".dockerfiles/db/init/seed_data_process_inputs.csv"
    process_outputs = ".dockerfiles/db/init/seed_data_process_outputs.csv"
    for file in [artifacts, processes, process_inputs, process_outputs]:
        if os.path.exists(file):
            os.remove(file)
    
    def writer(process: Process):
        with open(".dockerfiles/db/init/seed_data_artifacts.csv", "a") as f:
            for artifact in process.outputs:
                f.write(f"{artifact.type},{json.dumps(json.dumps(artifact.meta))}\n")
        
        with open(".dockerfiles/db/init/seed_data_processes.csv", "a") as f:
            f.write(f"{process.type},{json.dumps(json.dumps(process.meta))},{process.started_at},{process.finished_at}\n")
        
        with open(".dockerfiles/db/init/seed_data_process_inputs.csv", "a") as f:
            for artifact in process.inputs:
                f.write(f"{process.index},{artifact.index}\n")
        
        with open(".dockerfiles/db/init/seed_data_process_outputs.csv", "a") as f:
            for artifact in process.outputs:
                f.write(f"{process.index},{artifact.index}\n")
    return writer

csv_writer = CSVWriter()


def create_process(type, meta, parents=None):
    if parents is None:
        parents = []
    started_after = max([datetime(2023,1,1)] + [p.finished_at for p in parents])
    started_at = random_date(started_after, max(datetime(2023, 3, 31), started_after+timedelta(days=1)))
    finished_at = random_date(started_at, started_at+timedelta(days=1))


    process = Process(
        type=type,
        meta=meta,
        started_at=started_at,
        finished_at=finished_at,
    )


    for output_type in process_outputs[type]:
        artifact = Artifact(
            type=output_type,
            meta={"size": random.randint(1000000, 1000000000)},
        )
        process.outputs.append(artifact)
    
    for parent in parents:
        process.inputs.extend([x for x in parent.outputs if x.type in process_inputs[type]])
    assert len(process.inputs) == len(set([x.index for x in process.inputs])), f"Process {type} has {len(process.inputs)} inputs, but should have {len(process_inputs[type])} inputs"

    csv_writer(process)
    return process 

#def generate_family()
sample_id = 2300000000


for i in range(root_processes):
    #is_trio = random.randint(0, ) > 4 # 20% chance of trio
    n_samples = random.choice([1,1,1,1,1,1,2,3,3,3,3,3,4,4])
    variant_calling_processes = []
    for sample in range(n_samples):
        sample_id += 1
        process = create_process("sequencing", {"sample_id": sample_id})
        
        num_alignments = random.randint(1, 2)
        for alignment in range(num_alignments):
            alignment_process = create_process("alignment", {"reference_genome": "GRCh37"}, [process])
            qc_process = create_process("qc", {"threshold": random.randint(80,99)}, [alignment_process])
            #num_variant_calling = random.randint(1, 2)
            variant_callers = ["dragen_variant_calling", "dragen_cnv_variant_calling", "gatk_variant_calling"]
            num_variant_callers = random.randint(1, len(variant_callers))

            for variant_caller in variant_callers[:num_variant_callers]:
                variant_calling_process = create_process(variant_caller, {"caller_version": ".".join(str(random.randint(100,999)))}, [alignment_process])
                variant_calling_processes.append(variant_calling_process)
                annotation_process = create_process("annotation", {"data_version": ".".join(str(random.randint(100,999)))}, [variant_calling_process])
    

    if n_samples > 1:
        joint_calling_process = create_process("dragen_joint_calling", {"caller_version": ".".join(str(random.randint(100,999)))}, [x for x in variant_calling_processes if x.type == "dragen_variant_calling"])
        annotation_process = create_process("annotation", {"data_version": ".".join(str(random.randint(100,999)))}, [joint_calling_process])
    

            