COPY process (process_type, meta, started_on, ended_on) FROM '/docker-entrypoint-initdb.d/seed_data_processes.csv' DELIMITER ',' CSV  ESCAPE E'\\';
COPY artifact (artifact_type, meta) FROM '/docker-entrypoint-initdb.d/seed_data_artifacts.csv' DELIMITER ',' CSV  ESCAPE E'\\';
COPY process_input (process_id, artifact_id) FROM '/docker-entrypoint-initdb.d/seed_data_process_inputs.csv' DELIMITER ',' CSV  ESCAPE E'\\';
COPY process_output (process_id, artifact_id) FROM '/docker-entrypoint-initdb.d/seed_data_process_outputs.csv' DELIMITER ',' CSV  ESCAPE E'\\';
