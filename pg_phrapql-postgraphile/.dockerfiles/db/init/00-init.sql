create extension pg_graphql;

create role anon;

grant usage on schema public to anon;
alter default privileges in schema public grant all on tables to anon;
alter default privileges in schema public grant all on functions to anon;
alter default privileges in schema public grant all on sequences to anon;

grant usage on schema graphql to anon;
grant all on function graphql.resolve to anon;

alter default privileges in schema graphql grant all on tables to anon;
alter default privileges in schema graphql grant all on functions to anon;
alter default privileges in schema graphql grant all on sequences to anon;


-- GraphQL Entrypoint
create function graphql(
    "operationName" text default null,
    query text default null,
    variables jsonb default null,
    extensions jsonb default null
)
    returns jsonb
    language sql
as $$
    select graphql.resolve(
        query := query,
        variables := coalesce(variables, '{}'),
        "operationName" := "operationName",
        extensions := extensions
    );
$$;

-- TODO: migrations
CREATE TABLE artifact (
    -- id UUID DEFAULT gen_random_uuid() PRIMARY KEY,
    id INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    artifact_type TEXT,
    meta JSONB,
    created_on TIMESTAMPTZ DEFAULT NOW()
);

CREATE TABLE process (
    --id UUID DEFAULT gen_random_uuid() PRIMARY KEY,
    id INTEGER GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    process_type TEXT,
    meta JSONB,
    started_on TIMESTAMPTZ DEFAULT NOW(),
    ended_on TIMESTAMPTZ
);

CREATE TABLE process_input (
    process_id INTEGER REFERENCES process(id),
    artifact_id INTEGER REFERENCES artifact(id),
    PRIMARY KEY (process_id, artifact_id)
);

CREATE TABLE process_output (
    process_id INTEGER REFERENCES process(id),
    artifact_id INTEGER REFERENCES artifact(id),
    PRIMARY KEY (process_id, artifact_id)
);
