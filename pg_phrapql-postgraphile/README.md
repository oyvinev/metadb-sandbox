# Dev

1. Run `git submodule init; git submodule update`
2. Run `python generate_seed_data.py`
3. Run `docker-compose up`
4. Go to http://localhost:4000 to perform graphql queries (using pg_graphql)
5. Go to http://localhost:5000/graphiql to perform queries using postgraphile
