SELECT count(*) process_count FROM process;
SELECT count(*) artifact_count FROM artifact;


WITH RECURSIVE p(id) AS (
    SELECT id FROM process WHERE id = id
    UNION ALL
    SELECT process.id FROM p
    JOIN process_output ON process_output.process_id = process.id
    JOIN process_input ON process_input.process_id = id
)
SELECT count(10) FROM p;

WITH RECURSIVE t(n) AS (
    VALUES (1)
  UNION ALL
    SELECT n+1 FROM t WHERE n < 100
)
SELECT sum(n) FROM t;

WITH RECURSIVE ancestors(id, process_type, meta, depth, path) AS (
    SELECT p.id, p.process_type, p.meta, 1::INT AS depth, p.id::TEXT AS path 
    FROM process AS p 
    LEFT OUTER JOIN process_input AS pi ON pi.process_id = p.id
    LEFT OUTER JOIN process_output AS po ON po.process_id = pi.process_id
    WHERE po.process_id IS NULL
UNION ALL
    SELECT c.id, c.process_type, c.meta, p.depth + 1 AS depth, 
        (p.path || '->' || c.id::TEXT) 
    FROM ancestors AS p, process AS c 
    WHERE c.id = p.id
)
SELECT * FROM ancestors AS n WHERE n.id = 4;
