// Case 1: Single study, one run

// Create sequencing process
CREATE (p:Process:Sequencing{
    project: "wgs98", 
    instrument: "NovaSeq600",
    flowcell: "flowcellId",
    lane: 1
})
MERGE (p)-[:OUTPUTS]->(sa:Artifact{id:1})
MERGE (sa)-[:HAS_FILE]->(:File {
    extension: "fastq",
    path: "/data/UUID/sequencing1/1_1.fastq",
    timestamp: datetime(),
    md5: "blabla"
})
MERGE (sa)-[:HAS_FILE]->(:File {
    extension: "fastq",
    path: "/data/UUID/sequencing1/1_2.fastq",
    timestamp: datetime(),
    md5: "blabla"
})

// Create calling process
// MATCH (sa:Artifact{id:1})
MERGE (sa)-[:INPUTS]->(:Process:Calling{pipeline: "Dragen"})-[:OUTPUTS]->(:Artifact)-[:HAS_FILE]->(:File {
    extension: "vcf",
    path: "/data/UUID/calling1/1.vcf",
    timestamp: datetime(),
    md5: "blabla"
})

// Create annotation process
WITH sa
MATCH (sa)-[*]->(:Calling)-[:OUTPUTS]->(ca:Artifact)
MERGE (ca)-[:INPUTS]->(aa:Process:Annotation)-[:OUTPUTS]->(:Artifact)-[:HAS_FILE]->(:File {
    extension: "anno",
    path: "/data/UUID/annotation1/1.anno",
    timestamp: datetime(),
    md5: "blabla"
})
// Add interpretation process
WITH sa
MATCH (sa)-[*]->(:Annotation)-[:OUTPUTS]->(aa:Artifact)
MERGE (aa)-[:INPUTS]->(:Process:Interpretation)-[:OUTPUTS]->(:Artifact)-[:HAS_FILE]->(:File{
    extension: "ella",
    path: "/data/UUID/interpretation1/1.ella",
    timestamp: datetime(),
    md5: "blabla"
})


// 1
MATCH (gs:GeneticStudy{id:1})--(a:Sequencing)--(r:Result)--(f:File {extension: "fastq"})
RETURN a, f

MATCH (gs:GeneticStudy{id:1})-[:COMPOSED_BY]->(a:Calling)-[:HAS_RESULT]->(r:Result)-[:HAS_FILE]->(f:File{extension:"vcf"})
RETURN last(labels(a)), f.path as file

// 2 
MATCH (gs:GeneticStudy{id:1})-[:COMPOSED_BY]->(a:Analysis)-[:HAS_RESULT]->(r:Result)
RETURN last(LABELS(a)), r.timestamp
ORDER BY r.timestamp

// Get files of the last successful analysis

MATCH (gs:GeneticStudy{id:1})-[:COMPOSED_BY]->(a:Analysis)-[:HAS_RESULT]->(r:Result)-[:HAS_FILE]->(f:File)
RETURN last(LABELS(a)) as analysis, r.timestamp as time, f.path as file
ORDER BY time

// Get files of certain type for the last successful analysis
MATCH (gs:GeneticStudy{id:1})-[:COMPOSED_BY]->(a:Analysis)-[:HAS_RESULT]->(r:Result)-[:HAS_FILE]->(f:File{extension:"fastq"})
RETURN last(LABELS(a)) as analysis, r.timestamp as time, f.path as file
ORDER BY time

// 5 
// Get files of a certain type for a person in any study
MATCH (p:Person{id:11})-[:*]-(f:File {extension:"vcf"})
RETURN f

// Get members of a family
MATCH (proband:Person)-[*]-(p2:Person)
RETURN relative

// Get files of a type for all relatives of a person
MATCH (proband:Person)-[:IS_RELATIVE*]-(relative:Person)
WITH relative
MATCH (relative)<-[:HAS_PARTICIPANT]-(gs:GeneticStudy)--(:Analysis)--(:Result)--(f:File {extension:"vcf"})
RETURN f

MATCH (gs:GeneticStudy)--(a:Sequencing)
RETURN DISTINCT gs, a

// Get people that in any of its samples had a variant called
MATCH (p:Person)<-[:HAS_PARTICIPANT{role:"proband"}]-(gs:GeneticStudy)--(:Calling)--(:Result)--(v:Variant{id:"rs12345"})
RETURN DISTINCT p.Name

// Get people with a certain gene variant in their blood sample
MATCH (p:Person)--(ts:TissueSample{type:"blood"})<-[:DERIVES_FROM]-(dnae:Extraction)<-[:COMPOSED_BY]-(:Result)<-[:HAS_INPUT]-(s:Sequencing)
WITH DISTINCT p, s
OPTIONAL MATCH (s)-[:HAS_OUTPUT]->(rs:Result)-[:HAS_INPUT]->(:Calling)-[:HAS_OUTPUT]->(:Result)--(v:Variant{id:"rs12345"})
RETURN DISTINCT p.Name, collect(s)

// // Get people with a certain gene variant in their tumor sample
MATCH (p:Person)--(ts:TissueSample{type:"tumor"})<-[:DERIVES_FROM]-(dnae:Extraction)<-[:COMPOSED_BY]-(:Result)<-[:HAS_INPUT]-(:Sequencing)
MATCH (:Sequencing)-[:HAS_OUTPUT]->(:Result)-[:HAS_INPUT]->(:Calling)-[:HAS_OUTPUT]->(:Result)--(:Variant{id:"rs12345"})
RETURN DISTINCT p.Name

// We can reduce the steps by adding a connection between analysis nodes to a particular extraction

// Get all extractions that had a variant called; then get the persons ownning the extractions
MATCH (c:Calling)-[:HAS_OUTPUT]->(:Result)--(:Variant{id:"rs12345"})
WITH c
MATCH (p:Person)--(:TissueSample{type:"tumor"})<-[:DERIVES_FROM]-(:Extraction)<-[:BELONGS_TO]-(c)
RETURN DISTINCT p

MATCH (p:Person)--(:TissueSample{type:"tumor"})<-[:DERIVES_FROM]-(:Extraction)<-[:BELONGS_TO]-(:Calling)-[:HAS_OUTPUT]->(:Result)--(:Variant{id:"rs12345"})
RETURN DISTINCT p

// Get all family members with a variant in their blood
MATCH (proband:Person)-[:IS_RELATIVE*]-(relative:Person)
WITH relative
MATCH (relative)--(:TissueSample{type:"blood"})<-[:DERIVES_FROM]-(:Extraction)<-[:BELONGS_TO]-(c:Calling)-[:HAS_OUTPUT]->(:Result)--(:Variant{id:"rs12345"})
RETURN DISTINCT relative 

// Cases when a person returns for studies every year

// When a family did a study during pregnancy and then another some years later

// Then the sequencing results of an individual may be used in multiple studies

