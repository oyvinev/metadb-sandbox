// Case 2: Single study, multiple runs

// Create sequencing process
CREATE (p:Process:Sequencing{
    project: "wgs1", 
    instrument: "NovaSeq600",
    flowcell: "flowcellId",
    lane: 1
})
MERGE (p)-[:OUTPUTS]->(sa:Artifact{id:1})
MERGE (sa)-[:HAS_FILE]->(:File {
    extension: "fastq",
    path: "/data/UUID/sequencing1/1_1.fastq",
    timestamp: datetime(),
    md5: "blabla"
})
MERGE (sa)-[:HAS_FILE]->(:File {
    extension: "fastq",
    path: "/data/UUID/sequencing1/1_2.fastq",
    timestamp: datetime(),
    md5: "blabla"
})

// Create calling process
// MATCH (sa:Artifact{id:1})
MERGE (sa)-[:INPUTS]->(:Process:Calling{pipeline: "Dragen"})-[:OUTPUTS]->(:Artifact)-[:HAS_FILE]->(:File {
    extension: "vcf",
    path: "/data/UUID/calling1/1.vcf",
    timestamp: datetime(),
    md5: "blabla"
})

// Create annotation process
WITH sa
MATCH (sa)-[*]->(:Calling)-[:OUTPUTS]->(ca:Artifact)
MERGE (ca)-[:INPUTS]->(aa:Process:Annotation)-[:OUTPUTS]->(:Artifact)-[:HAS_FILE]->(:File {
    extension: "anno",
    path: "/data/UUID/annotation1/1.anno",
    timestamp: datetime(),
    md5: "blabla"
})
// Add interpretation process
WITH sa
MATCH (sa)-[*]->(:Annotation)-[:OUTPUTS]->(aa:Artifact)
MERGE (aa)-[:INPUTS]->(:Process:Interpretation)-[:OUTPUTS]->(:Artifact)-[:HAS_FILE]->(:File{
    extension: "ella",
    path: "/data/UUID/interpretation1/1.ella",
    timestamp: datetime(),
    md5: "blabla"
})

// Add new sequencing
CREATE (s2:Process:Sequencing{
    project: "wgs2", 
    instrument: "MiSeqV3",
    flowcell: "flowcellId",
    lane: 2
})
MERGE (s2)-[:OUTPUTS]->(sa2:Artifact{id:2})
MERGE (sa2)-[:HAS_FILE]->(:File {
    extension: "fastq",
    path: "/data/UUID/sequencing2/part1.fastq",
    timestamp: datetime(),
    md5: "blabla"
})
MERGE (sa2)-[:HAS_FILE]->(:File {
    extension: "fastq",
    path: "/data/UUID/sequencing2/part2.fastq",
    timestamp: datetime(),
    md5: "blabla"
})

// Create calling process
MATCH (sa2:Artifact) WHERE sa2.id = 2
WITH sa2
MERGE (sa2)-[:INPUTS]->(:Process:Calling{pipeline: "Dragen"})-[:OUTPUTS]->(:Artifact)-[:HAS_FILE]->(:File {
    extension: "vcf",
    path: "/data/UUID/calling2/file.vcf",
    timestamp: datetime(),
    md5: "blabla"
})
MERGE (sa2)-[:INPUTS]->(:Process:Calling{pipeline: "GATK"})-[:OUTPUTS]->(:Artifact)-[:HAS_FILE]->(:File {
    extension: "vcf",
    path: "/data/UUID/calling3/file.vcf",
    timestamp: datetime(),
    md5: "blabla"
})

// Create annotation process
MATCH (sa2:Artifact) WHERE sa2.id = 2
WITH sa2
MATCH (sa2)-[*]->(c:Calling)-[:OUTPUTS]->(ca:Artifact)
WHERE c.pipeline = "Dragen"
MERGE (ca)-[:INPUTS]->(aa:Process:Annotation)-[:OUTPUTS]->(:Artifact)-[:HAS_FILE]->(:File {
    extension: "anno",
    path: "/data/UUID/annotation2/file.anno",
    timestamp: datetime(),
    md5: "blabla"
})

MATCH (sa2:Artifact)-[*]->(c:Calling)-[:OUTPUTS]->(ca:Artifact)
WHERE sa2.id = 2 AND c.pipeline = "GATK" // Or any other condition
MERGE (ca)-[:INPUTS]->(aa:Process:Annotation)-[:OUTPUTS]->(:Artifact)-[:HAS_FILE]->(:File {
    extension: "anno",
    path: "/data/UUID/annotation3/file.anno",
    timestamp: datetime(),
    md5: "blabla"
})

// Add interpretation process
MATCH (sa2:Artifact)-[*]->(c:Calling)-[*]->(:Annotation)-[:OUTPUTS]->(aa:Artifact)
WHERE sa2.id = 2 AND c.pipeline = "GATK"
MERGE (aa)-[:INPUTS]->(:Process:Interpretation)-[:OUTPUTS]->(:Artifact)-[:HAS_FILE]->(:File{
    extension: "ella",
    path: "/data/UUID/interpretation2/1.ella",
    timestamp: datetime(),
    md5: "blabla"
})