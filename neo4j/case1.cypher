// Case 1: Single study, one run

// Create sequencing process
CREATE (p:Process:Sequencing{
    project: "wgs98", 
    instrument: "NovaSeq600",
    flowcell: "flowcellId",
    lane: 1
})
MERGE (p)-[:OUTPUTS]->(sa:Artifact{id:1})
MERGE (sa)-[:HAS_FILE]->(:File {
    extension: "fastq",
    path: "/data/UUID/sequencing1/1_1.fastq",
    timestamp: datetime(),
    md5: "blabla"
})
MERGE (sa)-[:HAS_FILE]->(:File {
    extension: "fastq",
    path: "/data/UUID/sequencing1/1_2.fastq",
    timestamp: datetime(),
    md5: "blabla"
})

// Create calling process
// MATCH (sa:Artifact{id:1})
MERGE (sa)-[:INPUTS]->(:Process:Calling{pipeline: "Dragen"})-[:OUTPUTS]->(:Artifact)-[:HAS_FILE]->(:File {
    extension: "vcf",
    path: "/data/UUID/calling1/1.vcf",
    timestamp: datetime(),
    md5: "blabla"
})

// Create annotation process
WITH sa
MATCH (sa)-[*]->(:Calling)-[:OUTPUTS]->(ca:Artifact)
MERGE (ca)-[:INPUTS]->(aa:Process:Annotation)-[:OUTPUTS]->(:Artifact)-[:HAS_FILE]->(:File {
    extension: "anno",
    path: "/data/UUID/annotation1/1.anno",
    timestamp: datetime(),
    md5: "blabla"
})
// Add interpretation process
WITH sa
MATCH (sa)-[*]->(:Annotation)-[:OUTPUTS]->(aa:Artifact)
MERGE (aa)-[:INPUTS]->(:Process:Interpretation)-[:OUTPUTS]->(:Artifact)-[:HAS_FILE]->(:File{
    extension: "ella",
    path: "/data/UUID/interpretation1/1.ella",
    timestamp: datetime(),
    md5: "blabla"
})