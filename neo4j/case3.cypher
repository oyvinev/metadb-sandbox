UNWIND range(1,2) as test
// Insert sequencing run process
CREATE (s:Process:Sequencing{
    runId: test,
    instrument_name: "NovaSeq600",
    instrument_type: "WGS",
    status: "running",
    project: "wgs3"
})
// Insert artifacts resulting from sequencing
WITH s, test
UNWIND range(1, 4) AS labId
MERGE (s)-[:OUTPUTS]->(a:Artifact{
    id: randomUUID(),
    runId: test,
    flowcell: "flowcellId",
    lane: 1,
    index: "index",
    labId: labId,
    labprepType: "labprep"
})
// Insert couple of files for each sequencing dataset
MERGE (a)-[:HAS_FILE]->(:File {
    extension: "fastq",
    path: "/data/" + a.id + "/reads1.fastq",
    timestamp: datetime(),
    md5: "blabla"
})
MERGE (a)-[:HAS_FILE]->(:File {
    extension: "fastq",
    path: "/data/" + a.id + "/reads2.fastq",
    timestamp: datetime(),
    md5: "blabla"
})
// Insert analysis results
MERGE (a)-[:INPUTS]->(base:Process:Basepipe)
// Create 4 different artifacts for each basepipe process
MERGE (base)-[:OUTPUTS]->(:Artifact{type:"coverage"})-[:HAS_FILE]->(:File {
    extension: "d4",
    path: "/data/" + a.id + "/coverage.d4",
    timestamp: datetime(),
    md5: "blabla"
})
MERGE (base)-[:OUTPUTS]->(:Artifact{type:"qc"})-[:HAS_FILE]->(:File {
    extension: "json",
    path: "/data/" + a.id + "/qc_result.json",
    timestamp: datetime(),
    md5: "blabla"
})
MERGE (base)-[:OUTPUTS]->(a_vc:Artifact{type:"calling"})-[:HAS_FILE]->(:File {
    extension: "vcf",
    path: "/data/" + a.id + "/final.vcf",
    timestamp: datetime(),
    md5: "blabla"
})
MERGE (a_vc)-[:HAS_FILE]->(:File {
    extension: "gvcf",
    path: "/data/" + a.id + "/file.gvcf",
    timestamp: datetime(),
    md5: "blabla"
})
MERGE (base)-[:OUTPUTS]->(:Artifact{type:"alignment"})-[:HAS_FILE]->(:File {
    extension: "bam",
    path: "/data/" + a.id + "/file.bam",
    timestamp: datetime(),
    md5: "blabla"
})
MERGE (trio:Process:Triopipe{runId:test})
WITH a, trio
MATCH (a)-[*]->(a_c:Artifact{type:"calling"})
WHERE a.labId IN [1, 2, 3]
WITH a, a_c, trio
MATCH (a)-[*]->(a_a:Artifact{type:"alignment"}) 
MERGE (a_c)-[:INPUTS]->(trio)
MERGE (a_a)-[:INPUTS]->(trio)
MERGE (trio)-[:OUTPUTS]->(a_t:Artifact)
MERGE (a_t)-[:INPUTS]->(anno:Process:Annopipe)
MERGE (anno)-[:OUTPUTS]->(a_anno_qc:Artifact{type:"annopipeQc"})
MERGE (a_anno_qc)-[:HAS_FILE]->(:File {
    extension: "md",
    path: "/data/what_here/warnings.md",
    timestamp: datetime(),
    md5: "blabla"
})
MERGE (a_anno_qc)-[:HAS_FILE]->(:File {
    extension: "qc",
    path: "/data/what_here/annopipe.qc",
    timestamp: datetime(),
    md5: "blabla"
})
MERGE (anno)-[:OUTPUTS]->(a_anno:Artifact{type:"annopipe"})
MERGE (a_anno)-[:HAS_FILE]->(:File {
    extension: "vcf",
    path: "/data/what_here/annotated.vcf",
    timestamp: datetime(),
    md5: "blabla"
})
MERGE (a_anno)-[:HAS_FILE]->(:File {
    extension: "bed",
    path: "/data/what_here/genomic_tracks.bed",
    timestamp: datetime(),
    md5: "blabla"
})
MERGE (a_anno)-[:INPUTS]->(ella:Process:EllaImport)

WITH a
MATCH (a)-[*]->(a_c:Artifact{type:"calling"})
WHERE a.labId = 1
CREATE (single:Process:Annopipe)
CREATE (ella2:Process:EllaImport)
WITH a, a_c, single, ella2
MATCH (a)-[*]->(a_a:Artifact{type:"alignment"}) 
MERGE (a_c)-[:INPUTS]->(single)
MERGE (a_a)-[:INPUTS]->(single)
MERGE (single)-[:OUTPUTS]->(a_anno_qc:Artifact{type:"annopipeQc"})
MERGE (a_anno_qc)-[:HAS_FILE]->(:File {
    extension: "md",
    path: "/data/what_here/warnings.md",
    timestamp: datetime(),
    md5: "blabla"
})
MERGE (a_anno_qc)-[:HAS_FILE]->(:File {
    extension: "qc",
    path: "/data/what_here/annopipe.qc",
    timestamp: datetime(),
    md5: "blabla"
})
MERGE (single)-[:OUTPUTS]->(a_anno:Artifact{type:"annopipe"})
MERGE (a_anno)-[:HAS_FILE]->(:File {
    extension: "vcf",
    path: "/data/what_here/annotated.vcf",
    timestamp: datetime(),
    md5: "blabla"
})
MERGE (a_anno)-[:HAS_FILE]->(:File {
    extension: "bed",
    path: "/data/what_here/genomic_tracks.bed",
    timestamp: datetime(),
    md5: "blabla"
})
MERGE (a_anno)-[:INPUTS]->(ella2)