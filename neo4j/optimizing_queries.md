# Query tuning

- It may be helpful to rephrase queries using knowledge about the domain, so that queries can be rephrased using knowledge about the application domain.

- The main goal is that only necessary data is retrieved from the graph.
- Data should get filtered out as early as possible, to reduce the amount of work needed to be done in later stages of query execution.

- Queries should avoid returning whole nodes and relationships in favour of selecting and returning only the data that is needed.

- Set an upper limit on variable length patterns, so they do not cover larger portions of the dataset than needed.

- Using parameters instead of literals minimizes de the resources used for the execution plan made by the Cypher query planner. This allows Cypher to re-use your queries instead of having to parse and build new execution plans.

- The execution plan is a tree-like structure where operators are combined. Each operator of the query performs a specific piece of work.

- Each operator takes as input zero or more rows, and produces as output zero or more rows. Meaning that the output of one operator is the input for the next operator.

- The execution plan begins at the leaf nodes of the tree. Lead nodes do not have inputs. Therefore they obtain data directly from the storage engine.

- Leaf nodes pipe their output to their parents and so on. The root node produces the final results of the query.